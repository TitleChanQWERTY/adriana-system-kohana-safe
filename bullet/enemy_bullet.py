import pygame
from group_pack import bullet_enemy_group


class SimpleBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, size, speed=5, pos=0):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))
        self.add(bullet_enemy_group)

        self.speedX = self.speedY = speed

        match pos:
            case 0:
                self.speedX = 0
                self.speedY = speed
            case 1:
                self.speedX = -speed
                self.speedY = 0
            case 2:
                self.speedX = 0
                self.speedY = -speed
            case 3:
                self.speedX = speed
                self.speedY = 0

    def update(self):
        if self.rect.x < -15 or self.rect.x > 1250 or self.rect.y > 730 or self.rect.y < -25:
            self.kill()
        self.rect.x += self.speedX
        self.rect.y += self.speedY
