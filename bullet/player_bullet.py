import pygame
from group_pack import bullet_group


class SimpleBullet(pygame.sprite.Sprite):
    def __init__(self, x, y, filename, speed, size):
        super().__init__()
        self.image = pygame.image.load(filename).convert_alpha()
        self.image = pygame.transform.scale(self.image, size)
        self.rect = self.image.get_rect(center=(x, y))

        self.add(bullet_group)

        self.speed = speed

    def update(self):
        if self.rect.x >= 1250:
            self.kill()
        self.rect.x += self.speed
