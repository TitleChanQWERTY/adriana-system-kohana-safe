from random import randint

import pygame
from config_script import WINDOW_SIZE
from bullet import player_bullet
import collider


class Player(pygame.sprite.Sprite):
    def __init__(self, x, y, speed):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/player/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (35, 30))
        self.rect = self.image.get_rect(center=(x, y))

        self.speed: int = speed

        self.Health: int = 5
        self.Damage: int = 1
        self.EXP: int = 0

        self.timeShoot = [0, 0, 0]

        self.SCORE: int = 0

    def check_exp(self):
        if self.EXP >= 45:
            self.Damage = int(self.EXP / 125 * 25)

    def update(self):

        key = pygame.key.get_pressed()

        self.shoot(key)
        self.move(key)

    def shoot(self, key):
        if key[pygame.K_k]:
            if self.timeShoot[0] >= 7:
                player_bullet.SimpleBullet(self.rect.x + 30, self.rect.centery, "ASSETS/sprite/bullet/player/1.png",
                                           randint(15, 20), (10, 10))
                self.timeShoot[0] = 0
            if self.EXP >= 50 and self.timeShoot[1] >= 15:
                player_bullet.SimpleBullet(self.rect.x + 15, self.rect.centery - 22, "ASSETS/sprite/bullet/player/2.png",
                                           11, (14, 12))
                player_bullet.SimpleBullet(self.rect.x + 15, self.rect.centery + 22,
                                           "ASSETS/sprite/bullet/player/2.png",
                                           11, (14, 12))
                self.timeShoot[1] = 0
            if self.EXP >= 85 and self.timeShoot[2] >= 10:
                player_bullet.SimpleBullet(self.rect.x + 15, self.rect.centery-9, "ASSETS/sprite/bullet/player/3.png",
                                           30, (11, 10))
                player_bullet.SimpleBullet(self.rect.x + 15, self.rect.centery+9, "ASSETS/sprite/bullet/player/3.png",
                                           30, (11, 10))
                self.timeShoot[2] = 0
            self.timeShoot[0] += 1
            self.timeShoot[1] += 1
            self.timeShoot[2] += 1

    def move(self, key):
        move_key_pressed_x = key[pygame.K_a] - key[pygame.K_d]
        move_key_pressed_y = key[pygame.K_w] - key[pygame.K_s]

        self.rect.x -= round(move_key_pressed_x * self.speed)
        self.rect.y -= round(move_key_pressed_y * self.speed)

        self.rect.x = max(1, min(WINDOW_SIZE[0] - 35, self.rect.x))
        self.rect.y = max(46, min(637, self.rect.y))

        if key[pygame.K_w]:
            self.image = pygame.image.load("ASSETS/sprite/player/2.png").convert_alpha()
        elif key[pygame.K_s]:
            self.image = pygame.image.load("ASSETS/sprite/player/2.png").convert_alpha()
            self.image = pygame.transform.flip(self.image, False, True)
        else:
            self.image = pygame.image.load("ASSETS/sprite/player/1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (35, 30))
        if collider.timeDamagePlayer < 120:
            self.image.set_alpha(100)
        else:
            self.image.set_alpha(300)
