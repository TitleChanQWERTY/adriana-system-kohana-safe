from random import randint

import pygame
import group_pack
import scene_config

timeDamagePlayer: int = 120


def item_collider():
    for item in group_pack.item_group:
        if scene_config.p.rect.colliderect(item.rect):
            if item.id == 0 or item.id == 1:
                scene_config.p.EXP += 5
                if item.id == 1:
                    scene_config.p.EXP += 5
                scene_config.p.SCORE += 25
                scene_config.p.check_exp()
                item.kill()


def bullet_collider():
    global timeDamagePlayer
    for bullet in group_pack.bullet_group:
        for enemy in group_pack.enemy_group:
            if bullet.rect.colliderect(enemy.rect):
                enemy.Health -= scene_config.p.Damage
                enemy.isTakeDamage = True

    if timeDamagePlayer >= 120:
        for bullet_enemy in group_pack.bullet_enemy_group:
            if scene_config.p.rect.collidepoint(bullet_enemy.rect.center):
                if scene_config.p.rect.x > 36:
                    scene_config.p.rect.x -= 35
                scene_config.p.EXP -= randint(1, 5)
                scene_config.p.Health -= 1
                timeDamagePlayer = 0
                bullet_enemy.kill()

    else:
        timeDamagePlayer += 1
