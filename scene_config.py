from random import randint

import pygame

import config_script
from player import Player

p = Player(120, config_script.WINDOW_SIZE[1] // 2, 7)

news_txt = ("На вулицях знаходять невідомі тіла людей з котрих висосили кров", "На вулиці +1 тепла та дує легкий вітер",
            "Була оновлена ліцензія для державних послуг", "Данна признала що Серсонатусу потрібна більша воєнна сила",
            "Усі лідери міст чистилища зберуться на конференції PSC", "Тепер собаки можуть мати друзів!",
            "В японському районі почався масовий розтріл. Це пов'язано із-за непорозуміня між жителлями цього району",
            "Тепер ігрові консолі нікому не потрібні. Усі геймери грають на комп'ютері",
            "X-CUSE не може розроблятися із-за нестачи інгредіентів. Представник компанії ProjectVAC каже що це ненадовго",
            "Адріана прийняла Божий закон про оплату податків за день до оплати. Раді міста Серсонатус це не сподобалось")

news_font = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 16)
selectNews = news_txt[randint(0, len(news_txt) - 1)]
news_txt_surf = news_font.render(selectNews, False, (255, 255, 255))
xPosNews = 2200
news_txt_rect = news_txt_surf.get_rect(center=(xPosNews, 710))

timeUpdateNews: int = 0


def updateNews():
    global timeUpdateNews, news_txt_surf, news_txt, xPosNews, news_txt_rect, selectNews
    if timeUpdateNews >= 1455:
        selectNews = news_txt[randint(0, len(news_txt) - 1)]
        xPosNews = 2200
        timeUpdateNews = 0
    xPosNews -= 2
    news_txt_surf = news_font.render(selectNews, False, (255, 255, 255))
    news_txt_rect = news_txt_surf.get_rect(center=(xPosNews, 709))

    timeUpdateNews += 1
