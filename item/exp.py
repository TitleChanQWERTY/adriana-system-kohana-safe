import pygame
from group_pack import item_group


class Exp(pygame.sprite.Sprite):
    def __init__(self, x, y, type_item):
        super().__init__()
        self.filename = ("ASSETS/sprite/item/exp/1.png", "ASSETS/sprite/item/exp/2.png")
        self.image = pygame.image.load(self.filename[type_item]).convert_alpha()
        self.image = pygame.transform.scale(self.image, (27, 27))
        self.rect = self.image.get_rect(center=(x, y))

        self.id: int = type_item

        self.x = x
        self.y = y

        self.add(item_group)

        self.angle: int = 0

    def update(self):
        if self.rect.x < -25:
            self.kill()
        self.x -= 3

        self.image = pygame.image.load(self.filename[self.id]).convert_alpha()
        self.image = pygame.transform.scale(self.image, (27, 27))
        self.angle += 3
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.rect = self.image.get_rect(center=(self.x, self.y))
