import pygame
import config_script
import scene_config
import group_pack
from collider import bullet_collider, item_collider

fontStat = pygame.font.Font("ASSETS/font/ABC_TypeWriterRussian.ttf", 20)

TEXT_SET_STAT = ("SCORE: ", "LIFE: ", "EXP: ", "damage: ")

if config_script.LANGUAGE == "UA":
    TEXT_SET_STAT = ("БАЛЛИ: ", "ЖИТТЯ: ", "ЕКСП: ", "урон: ")

ScoreTXT = fontStat.render(TEXT_SET_STAT[0] + str(scene_config.p.SCORE), False, (255, 255, 0))
LifeTXT = fontStat.render(TEXT_SET_STAT[1] + str(scene_config.p.Health), False, (255, 0, 0))
ExpTXT = fontStat.render(TEXT_SET_STAT[2] + str(scene_config.p.EXP), False, (155, 165, 255))

DamageTXT = fontStat.render(TEXT_SET_STAT[3] + str(scene_config.p.Damage), False, (255, 255, 255))


def updateTXT():
    global ScoreTXT, LifeTXT, DamageTXT, ExpTXT, TEXT_SET_STAT

    if config_script.LANGUAGE == "UA":
        TEXT_SET_STAT = ("БАЛЛИ: ", "ЖИТТЯ: ", "ЕКСП: ", "урон: ")

    ScoreTXT = fontStat.render(TEXT_SET_STAT[0] + str(scene_config.p.SCORE), False, (255, 255, 0))

    LifeTXT = fontStat.render(TEXT_SET_STAT[1] + str(scene_config.p.Health), False, (255, 0, 0))
    LifeTXT = pygame.transform.scale(LifeTXT, (100, 25))

    ExpTXT = fontStat.render(TEXT_SET_STAT[2] + str(scene_config.p.EXP), False, (155, 165, 255))

    DamageTXT = fontStat.render(TEXT_SET_STAT[3] + str(scene_config.p.Damage), False, (255, 255, 255))


def drawScene():
    config_script.sc.blit(scene_config.p.image, scene_config.p.rect)
    group_pack.bullet_group.draw(config_script.sc)
    group_pack.enemy_group.draw(config_script.sc)
    group_pack.bullet_enemy_group.draw(config_script.sc)
    group_pack.item_group.draw(config_script.sc)
    config_script.sc.blit(ScoreTXT, ScoreTXT.get_rect(center=(220, 17)))
    config_script.sc.blit(LifeTXT, LifeTXT.get_rect(center=(config_script.WINDOW_SIZE[0]//2, 25)))
    config_script.sc.blit(ExpTXT, ExpTXT.get_rect(center=(1040, 17)))
    config_script.sc.blit(DamageTXT, DamageTXT.get_rect(center=(config_script.WINDOW_SIZE[0] // 2, 689)))


def updateScene():
    item_collider()
    bullet_collider()
    scene_config.p.update()
    group_pack.bullet_group.update()
    group_pack.enemy_group.update()
    group_pack.bullet_enemy_group.update()
    group_pack.item_group.update()


def scene():
    for e in pygame.event.get():
        if e.type == pygame.QUIT:
            config_script.running = False

    config_script.sc.fill((0, 0, 0))
    drawScene()
    updateScene()
    updateTXT()
    scene_config.updateNews()
    config_script.sc.blit(scene_config.news_txt_surf, scene_config.news_txt_rect)
    pygame.draw.rect(config_script.sc, (255, 255, 255), (0, 45,
                                                         config_script.WINDOW_SIZE[0], 1))
    pygame.draw.rect(config_script.sc, (255, 255, 255), (0, 670,
                                                         config_script.WINDOW_SIZE[0], 1))
    pygame.display.flip()
    config_script.clock.tick(config_script.FPS)
