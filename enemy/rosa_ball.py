import math
from random import randint

import pygame
from group_pack import enemy_group
from scene_config import p
from bullet import enemy_bullet
from item import exp


class BallRosa(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/enemy/ball_rosa/ball_rosa.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (29, 29))
        self.rect = self.image.get_rect(center=(-500, -500))

        self.add(enemy_group)

        self.spawnTime: int = pygame.time.get_ticks()
        self.direction_time: int = randint(975, 1950)

        self.Health: int = 6

        self.timeStartMove: int = 0

        self.maxTimeMove: int = randint(670, 3500)

        self.luckyItem: int = randint(0, 1)

        self.isMove: bool = False

        self.isTakeDamage: bool = False
        self.timeDamage: int = 0

        self.timeShoot: int = 0

    def update(self):
        if not self.isMove:
            if self.timeStartMove >= self.maxTimeMove:
                self.rect.x = 1290
                self.rect.y = randint(100, 650)
                self.isMove = True
            self.timeStartMove += 1
        else:
            if self.timeShoot >= 146:
                for i in range(4):
                    enemy_bullet.SimpleBullet(self.rect.centerx-10, self.rect.centery, "ASSETS/sprite/bullet/enemy/3.png",
                                              (18, 18), 7, i)
                self.timeShoot = 0
            self.timeShoot += 1

            if self.Health <= 0 or self.rect.x <= -25:
                if self.Health <= 0:
                    p.SCORE += randint(5, 15)
                    if self.luckyItem == 0:
                        exp.Exp(self.rect.centerx, self.rect.centery, 0)
                self.Health = 6
                self.rect.x = -500
                self.timeShoot = 0
                self.rect.y = -500
                self.isMove = False
                self.timeStartMove = 0

            if self.isTakeDamage:
                self.image = pygame.image.load("ASSETS/sprite/enemy/ball_rosa/damage.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (33, 33))
                if self.timeDamage > 4.5:
                    self.image = pygame.image.load("ASSETS/sprite/enemy/ball_rosa/ball_rosa.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (29, 29))
                    self.isTakeDamage = False
                    self.timeDamage = 0
                self.timeDamage += 0.5

            self.move()

    def move(self):
        time = (self.spawnTime + pygame.time.get_ticks()) / self.direction_time * math.pi

        move_direction_y = round(math.copysign(2, math.sin(time)))
        move_direction_x = 1

        self.rect.x -= round(move_direction_x * 2)
        self.rect.y -= round(move_direction_y)

        self.rect.y = max(55, min(600, self.rect.y))
