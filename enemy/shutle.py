from random import randint

import pygame
from group_pack import enemy_group
from scene_config import p
from item import exp
from bullet import enemy_bullet


class Patrole(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("ASSETS/sprite/enemy/shutle/patrole /1.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (39, 33))
        self.rect = self.image.get_rect(center=(-600, -600))

        self.add(enemy_group)

        self.Health: int = 125

        self.timeStartMove: int = 0

        self.maxTimeMove: int = randint(855, 2950)

        self.isMove: bool = False

        self.isTakeDamage: bool = False
        self.timeDamage: int = 0

        self.timeShoot: int = 0

    def update(self):
        if not self.isMove:
            if self.timeStartMove >= self.maxTimeMove:
                self.rect.x = 1290
                self.rect.y = randint(100, 635)
                self.isMove = True
            self.timeStartMove += 1
        else:
            if self.rect.x < -45 or self.Health <= 0:
                if self.Health <= 0:
                    p.SCORE += randint(100, 200)
                    exp.Exp(self.rect.centerx, self.rect.centery, 1)
                self.Health = randint(125, 350)
                self.rect.x = -500
                self.timeShoot = 0
                self.rect.y = -500
                self.isMove = False
                self.timeStartMove = 0

            self.rect.x -= 4

            if self.timeShoot >= 65:
                enemy_bullet.SimpleBullet(self.rect.x - 30, self.rect.centery, "ASSETS/sprite/bullet/enemy/1.png",
                                          (29, 13), 5, 1)
                self.timeShoot = 0
            self.timeShoot += 1

            if self.isTakeDamage:
                self.image = pygame.image.load("ASSETS/sprite/enemy/shutle/patrole /damage.png").convert_alpha()
                self.image = pygame.transform.scale(self.image, (39, 33))
                if self.timeDamage > 4.5:
                    self.image = pygame.image.load("ASSETS/sprite/enemy/shutle/patrole /1.png").convert_alpha()
                    self.image = pygame.transform.scale(self.image, (39, 33))
                    self.isTakeDamage = False
                    self.timeDamage = 0
                self.timeDamage += 0.5
